# THIS IS MULTI-REPO
I am learning a lot of blockchain development stuff and for simplicity everything is stored here. 

## ./solidity/
Learning how to develop smart contract in Solidity.
- implementing some ERCs
- OOP in Solidity
- Gas spending knowledge

## ./solana/
Learning basics how to develop smart contract in Solana.

## ./vyper/
Learning basics how to develop smart contract in Vyber.

## ./rust/
Learning Rust.

## ./DeFi/
Learning about economy and finance is so much fun :)
