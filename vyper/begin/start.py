# @version 0.3.3

some_data: int128

@external
def __init__():
    self.some_data = 13


@external
def get_some_data() -> int128:
    return self.some_data
