// SPDX-License-Identifier: GPL-3.0
pragma solidity >=0.7.0 <0.9.0;

contract SendMoneyExample {

    uint public balance;
    address owner;
    bool public paused;

    constructor() {
        owner = msg.sender;
    }

    function receiveMoney() public payable {
        balance += msg.value;
    }

    function getBalance() public view returns (uint) {
        return address(this).balance;
    }

    function setPauserd(bool _paused) public {
        thisIsOwner();
        paused = _paused;
    }

    function withdrawMoney(address payable _to) public {
        thisIsOwner();
        payable(_to).transfer(getBalance());
    }

    function destroySmartContract(address payable _to) public {
        thisIsOwner();
        selfdestruct(_to);
    }

    function thisIsOwner() view public {
        require(msg.sender == owner, "You are not the owner.");
    }

}
