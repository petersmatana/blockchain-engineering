# Gas

How much **ether** is need to pay for a transaction? It is `gas spent * gas price` in amount of Ether, where:

- `gas` is a unit of consumption
- `gas spent` is total amount of `gas` used in a transaction
- `gas price` is how much `ether` am I willing to pay per `gas`

Transactions with higher gas price have higher priority to be included in a block. Unspent gas is refunded.

There are 2 upper bounds to the amount of gas you can spend:

- `gas limit` is max amount of gas you're willing to use for your transaction, set by you
- `block gas limit` is max amount of gas allowed in a block, set by the network


## optimization

1. Sore on Blockchain only important data. Other stuff or metadata is good to have elsewhere.
2. There are two types of libraries. Deployed libs which is good. Embedded libs which is not *good*. All functionality should be external.
3. ERC1167 - This is way how Smart Contract can be deployed more times than once.
4. Turn on Gas optimization in Solidity.
5. Use Events.
6. Use literal values instead of computed values. Hash calculation make outside of Smart Contract is good practice.
7. Do not copy array, use pointers.
8. Do not use for loop, use ranges.
9. Use well order of variables.
10. Use eth-gas-report.

# Upgradeable contracts

OpenZeppelin perspective: https://docs.openzeppelin.com/upgrades-plugins/1.x/

# Learning Solidity

(In Remix, need to turn on Solidity Static Analysis.)

## Transaction in Ethereum

There are three types of transaction that can be executed in Ethereum:

1. Transfer of Ether from one account to another: The accounts can be externally owned accounts or contract accounts. Following are the possible cases:
    - An externally owned account sending Ether to another externally owned account in a transaction
    - An externally owned account sending Ether to a contract account in a transaction
    - A contract account sending Ether to another contract account in a transaction
    - A contract account sending Ether to an externally owned account in a transaction 
2. Deployment of a smart contract: An externally owned account can deploy a contract using a transaction in EVM.
3. Using or invoking a function within a contract: Executing a function in a contract that changes state is considered a transaction in Ethereum. If executing a function does not change a state, it does not require a transaction.

## if VS. require VS. assert and gas consumption

```
// SPDX-License-Identifier: GPL-3.0
pragma solidity >=0.7.0 <0.9.0;

contract CompareComparators {

    address owner;

    constructor() {
        owner = msg.sender;
    }

    function ifKeyWord() public {
        if (owner != msg.sender) {
            emit Message("You are not the owner");
        }
        
    }

    function requiredKeyWord() public view {
        require(owner == msg.sender, "You are not the owner");
    }

    function assertKeyWork() public view {
        assert(owner == msg.sender);
    }

    event Message(
        string text
    );

}
```

Functions accept address which pass. Code deploy is just gas need to deploy.

|                  | Call if  | Call assert | Call require |
|------------------|----------|-------------|--------------|
| transaction cost | 23.353   |             |              |
| execute cost     | 23.353   | 23.375      | 23.397       |

Functions accept address which do not pass:

|                  | Call if | Call assert | Call require |
|------------------|---------|-------------|--------------|
| transaction cost | 25.155  |             |              |
| execute cost     | 25.155  | 23.404      | 23.686       |

# How to run Solidity on localhost

https://www.quicknode.com/guides/web3-sdks/how-to-setup-local-development-environment-for-solidity

## Install on Fedora

On Fedora all JS packages are globally store in `/lib/node_modules` or in `/usr/local/lib`.

I have problems to run command because it returns me: permissions denied. So I try this: https://docs.npmjs.com/resolving-eacces-permissions-errors-when-installing-packages-globally

`mkdir ~/.javascript_modules`

`npm config set prefix '~/.javascript_modules'`

`export PATH=~/.javascript_modules/bin:$PATH`

## Install on Ubuntu

All commands must be run in the root directory of project.

1. Truffle install

first need to install: apt install make

without root:
`npm install -g truffle`

binary file is install in: /node_modules/truffle/build/cli.bundled.js

but it is: `truffle init` and after that `truffle compile`

in my case I have to run truffle like this: `/home/smonty/Documents/node_packages/node_modules/truffle/build/cli.bundled.js`

If I start new version of compiler, it is automatically downloaded.

2. Run Ganache

download ganache appimage: https://trufflesuite.com/ganache/

change rights: `chmod u+x <ganache-build>.AppImage`

`./<ganache-build>.AppImage`

3. Configure Solidity project

configure truffle-config.js like this:

```
module.exports = {
  networks: {
    development: {
      host: "127.0.0.1",
      port: 7545,
      network_id: "*"
    }
  },
  compilers: {
    solc: {
      version: "0.8.1",   
    }
  },
};
```

and there is also need migration JS script:

```
const Migrations = artifacts.require("Migrations");

module.exports = function (deployer) {
  deployer.deploy(Migrations);
};
```

Output of migration should look like this:
```
Compiling your contracts...
===========================
> Everything is up to date, there is nothing to compile.


Starting migrations...
======================
> Network name:    'development'
> Network id:      5777
> Block gas limit: 6721975 (0x6691b7)


1_initial_migration.js
======================

   Deploying 'MyContract'
   ----------------------
   > transaction hash:    0x0dfacbe527ffdf4094e46248738fbf16fd27ff9bfb819a3ce42b4492f70cab8e
   > Blocks: 0            Seconds: 0
   > contract address:    0x6d1A5D9B0B65ca6F00eaAecF7Da16e3d1f925480
   > block number:        1
   > block timestamp:     1653413438
   > account:             0xaF870ac41C53CE5db7d0B16141eFebA9cf99E53b
   > balance:             99.99626498
   > gas used:            186751 (0x2d97f)
   > gas price:           20 gwei
   > value sent:          0 ETH
   > total cost:          0.00373502 ETH

   > Saving artifacts
   -------------------------------------
   > Total cost:          0.00373502 ETH

Summary
=======
> Total deployments:   1
> Final cost:          0.00373502 ETH
```

4. Deploy Smart Contract to Ganache
Right now Smart Contract should be compiled. To migrate run `truffle migrate`.

5. Console
There is also console which can be run with `truffle console`

## Run Remix locally with Docker

`docker pull remixproject/remix-ide`
`docker run -p 8080:80 remixproject/remix-ide`
`docker run -p 8080:80 -v /home/smonty/Documents/blockchain-engineering/solidity/:/home/ remixproject/remix-ide # no idea how to attach some volume`

in web browser run on: localhost:8080

But I do not know how to connect on local files.

# Run Remix locally via npm

`npm install -g @remix-project/remixd` - need to run with rood, idk why?

`remixd` - run remix ide without rood

`remixd -s /home/smonty/Documents/blockchain-engineering/solidity/ -u http://localhost:8080`

in browser it runs on localhost:8080

## docker version

In directory run-locally there is Dockerfile of truffle and ganache but this solution comes with solidity in version 0.5.x which is little bit of legacy.

# Ganache on localhost
I need to run Ganache on localhost. Here is approach how to do that:

## node.js
Actual LTS version is 16.14.0 which come from standard repo for Fedora.

```
$ sudo dnf install nodejs
$ node -v
v16.14.0
```

## Ganache
Just download Ganache.AppImage, run this:
```
chmod u+x ganache-file.AppImage
```
and then simply run:
```
./ganache-file.AppImage
```

## Slither
https://github.com/crytic/slither

0. In Ubuntu case install pip like: `apt install python3-pip python3-venv`

create python virtual env: `python3 -m venv <name>` and enable it like `source <name>/bin/activate`

1. run `pip3 install slither-analyzer`

....this is more complicated.. if realy need local, follow docker file...

### Slither with Docker
This maps my contracts into container where I can run Slither.
`docker run -v /home/smonty/Documents/learn-solidity:/home/ethsec/src -ti trailofbits/eth-security-toolbox`

# Using Truffle on localhost

# Ethereum Improvement Proposals

## ERC-20

It is standard for all smart contracts on the Ethereum blockchain for Fungible Token. Fungible Token is a Token which is the same of another Token. Same in type and value. In other words ERC-20 Token acts like the ETH. One Token is equal to all the other Tokens. This ERC provides interface of a standard API. This is basic functionality to work with the Tokens.

## ERC-165

Secures way how to publish and detect what interface smart contract implements.
