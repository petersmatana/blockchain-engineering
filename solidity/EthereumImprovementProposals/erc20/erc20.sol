// SPDX-License-Identifier: GPL-3.0
pragma solidity >=0.7.0 <0.9.0;

interface ERC20 {

    /**
      @notice Returns the amount of tokens in existence.
      @return Amount of tokens.
      */
    function totalSupply() external returns (uint256);

    /**
      @notice Returns the amount of tokens owned by account.
      @param _account Owners account address.
      @return Owners amount of tokens.
      */
    function balanceOf(address _account) external returns (uint256);

    /**
      @notice Move amount of tokens from sender to recipient. Returns true if transfer was successful, false othervise.
      @param _recipient Someone who accept tokens.
      @param _amount Amount of tokens which are sended.
      @return Returns true if transfer was successful, false othervise.
      */
    function transfer(address _recipient, uint256 _amount) external returns (bool);

    // TODO do not understand
    function allowance(address _owner, address _spender) external returns (uint256);

    // TODO do not understand correctly
    function approve(address _spender, uint256 _amount) external returns (bool);
}

contract Implementation is ERC20 {

  uint private _totalSuply; 

  function totalSupply() public view virtual override returns (uint256) {
    return _totalSuply;
  }

  function balanceOf(address _account) public view virtual override returns (uint256) {

  }

  function transfer(address _recipient, uint256 _amount) public view virtual override returns (bool) {

  }

  function allowance(address _owner, address _spender) public view virtual override returns (uint256) {

  }

  function approve(address _spender, uint256 _amount) public view virtual override returns (bool) {

  }

}
