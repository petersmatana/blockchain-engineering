// SPDX-License-Identifier: GPL-3.0
pragma solidity 0.8.13;

contract ERC1165 {
    address masterCopy;

    constructor(address _masterCopy) {
        masterCopy = _masterCopy;
    }

    function forward() external returns (bytes memory) {
        (bool success, bytes memory data) = masterCopy.delegatecall(msg.data);
        require(success);

        return data;
    }
}
