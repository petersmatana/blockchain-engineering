// SPDX-License-Identifier: GPL-3.0
pragma solidity >=0.7.0 <0.9.0;

contract B {

    mapping(bytes4 => bool) private _interfaces;

    // function calculateInterfaceId_v1() public pure returns (bytes4) {
    //     // keccak256 is hash function.
    //     // ^ is XOR
    //     return bytes4(keccak256("x()")) ^ bytes4(keccak256("y()")) ^ bytes4(keccak256("z()"));
    // }

    // function calculateInterfaceId_v2() public pure returns (bytes4) {
    //     B b;
    //     return b.x.selector ^ b.y.selector ^ b.x.selector;
    // }

    // function getHash() public pure returns (bytes4) {
    //     // 0xa56dfe4a
    //     B b;
    //     return b.x.selector ^ b.y.selector ^ b.x.selector;
    // }

    constructor() {
        /*
            What is happening in calculateInterfaceId_v1() and calculateInterfaceId_v2()
            is done here.
        */
        B b;
        _interfaces[b.x.selector ^ b.y.selector ^ b.x.selector] = true;
        _interfaces[0x01ffc9a7] = true;
    }

    /**
        @notice This is standart function to publish this interface. If this smart contract
            will be ERC-165 compliant also this function have to be hashed. From official docs
            this hash is: 0x01ffc9a7.
    */
    function supportsInterface(bytes4 interfaceID) external view returns (bool) {
        return _interfaces[interfaceID];
    } 

    function x() public {
        emit Message("contract B function x()");
    }

    function y() public {
        emit Message("contract B function y()");
    }

    function z() public {
        emit Message("contract B function z()");
    }

    event Message(
        string message
    );
}

interface interfaceB {
    function x() external;
    function y() external;
    function z() external;
}

contract CallableInterface {
  bytes4 internal constant STORE_INTERFACE_ID = 0x75b24222;
}

contract A is CallableInterface {
    function callX() public {
        B b;
        if (b.supportsInterface(0xa56dfe4a||0x01ffc9a7)) {
            b.x();
        } else {
            revert("contract x is not exists");
        }
    }
}

