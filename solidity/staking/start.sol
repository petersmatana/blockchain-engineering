// SPDX-License-Identifier: GPL-3.0
pragma solidity 0.8.14;

// https://itnext.io/creating-a-inheritable-staking-contract-in-solidity-7804ae2d7a32

import "@openzeppelin/contracts/access/Ownable.sol";

contract Stakeable {
    constructor() {
        stakeholders.push();
    }

    struct Stake {
        address user;
        uint256 amount;
        uint256 since;
    }

    struct Stakeholder {
        address user;
        Stake[] address_stakes;
    }

    Stakeholder[] internal stakeholders;

    mapping(address => uint256) internal stakes;

    event Staked(
        address indexed user,
        uint256 amount,
        uint256 index,
        uint256 timestamp
    );

    function _addStakeholder(address _staker) internal returns (uint256) {
        stakeholders.push();

        uint256 userIndex = stakeholders.length - 1;
        stakeholders[userIndex].user = _staker;

        stakes[_staker] = userIndex;

        return userIndex;
    }

    function _stake(uint256 _amount) internal view {
        require(_amount > 0, "Can not stake nothing.");

        uint256 index = stakes[msg.sender];
    }
}

contract DevToken is Ownable, Stakeable {

}
