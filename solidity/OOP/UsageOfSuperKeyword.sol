// SPDX-License-Identifier: GPL-3.0
pragma solidity 0.8.13;

contract Parrent {
    function f() public virtual returns (uint256) {
        return 13;
    }

    modifier onlyOwner() {
        _;
    }

    function renounceOwnership() public virtual onlyOwner {
        emit Message("Parrent - renounceOwnership");
    }

    function owner() public view virtual returns (address) {
        return address(0);
    }

    event Message(
        string text
    );
}

contract Child is Parrent {
    // correct usage of super keyword
    function f() public virtual override returns (uint256) {
        return super.f();
    }

    // correct usage of super keyword
    function renounceOwnership() public virtual override {
        super.renounceOwnership();
        emit Message("Child - renounceOwnership");
    }

    function owner() public view override returns (address) {
        return super.owner();
    }

    // If I would not like to have same function name but
    // be able to call code from Parrent I will do it like this:
    function diferentNameToF() external returns (uint256) {
        return new Parrent().f();
    }
}
