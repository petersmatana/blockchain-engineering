// SPDX-License-Identifier: GPL-3.0
pragma solidity 0.8.13;

interface StorageInterface {
    function getValue() external view returns (uint256);
    function setValue(uint256 _v) external;
}

// instance of this smart contract have some address
contract Store is StorageInterface {
    address internal owner;
    uint256 internal value;

    function getValue() external view returns (uint256) {
        return value;
    }

    function setValue(uint256 _v) external {
        value = _v;
    }
}

// instance of this smart contract also have some address but
// in argument of readFromStore is need to pas adress of Store contract
contract StoreReader {
    function readFromStore(address store) external view returns (uint256) {
        // something like casting input address into StorageInterface
        return StorageInterface(store).getValue();
    }
}
