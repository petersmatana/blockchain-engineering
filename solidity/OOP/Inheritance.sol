// SPDX-License-Identifier: GPL-3.0
pragma solidity 0.8.13;

// https://solidity-by-example.org/inheritance/
/* Graph of inheritance
    A
   / \
  B   C
 / \ /
F  D,E
*/

contract ContractA {
  function foo() virtual public {
    emit Message("foo from ContractA");
  }

  event Message(
    string message
  );
}

contract ContractB {
  function foo() virtual public {
    emit Message("foo from ContractB");
  }

  event Message(
    string message
  );
}

contract Inheritance is ContractA {
  function foo() public override (ContractA) {
    emit Message("foo from Inheritance");
  }
  // event Message(
  //   string message
  // );
}
