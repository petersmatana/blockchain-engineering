# Keywords

## public
Function can be call outside of the contrat. Solidity immediately copies array arguments to memory. Alocating memory is expensive so it is more gas consumption.
Public function should be called internally. When variable is public, solidity creates getter for it.

```
pragma solidity 0.8.13;

contract PublicFunction {
    function foo() public {
    }
}
```
|||
|--|--|
| gas | 80000000 gas |
| transaction cost | 76975 gas | 
| execution cost | 76975 gas |

## external
Function doing the same thing like `public`. External functions can read directly from calldata. Directly access to memory is cheap.
External function should be called externally.

``` 
pragma solidity 0.8.13;

contract PublicFunction {
    function foo() external {
    }
}
```
|||
|--|--|
| gas | 80000000 gas |
| transaction cost | 21186 gas | 
| execution cost | 21186 gas |

## internal

Internal state variables can only be accessed from within the contract they are defined in and in derived contracts. They cannot be accessed externally. This is the default visibility level for state variables.

## private

Private functions are like internal ones but they are not visible in derived contracts.

## pure

Pure function do not modify state. Pure is more strict than view - pure can not read the state. What violate that function can not be pure:

### Reading state variables
```
pragma solidity 0.8.13;

contract Contract {
    address internal owner;

    function foo() public pure {
        address x = owner; // Function declared as pure, but this expression (potentially) reads from the environment or state and thus requires "view".
    }
}
```

### Accessing address(this).balance or \<address\>.balance
```
pragma solidity 0.8.13;

contract Contract {
    address internal owner;

    function foo() public pure {
        if (address(this).balance > 0) {} // Function declared as pure, but this expression (potentially) reads from the environment or state and thus requires "view".
    }
}
```

### Accessing any of the special variable of block, tx, msg (msg.sig and msg.data can be read).
```
pragma solidity 0.8.13;

contract Contract {
    address internal owner;

    function foo() public pure {
        if (msg.value > 0) {} // Function declared as pure, but this expression (potentially) reads from the environment or state and thus requires "view".
    }
}
```

### Calling any function not marked pure.
```
pragma solidity 0.8.13;

contract Contract {
    address internal owner;

    function foo() public pure {
        bar(); // Function cannot be declared as pure because this expression (potentially) modifies the state.
    }

    function bar() private {}
}
```

### Using inline assembly that contains certain opcodes.

*// todo*

## view

Functions can be declared `view` in which case they promise not to modify the state. View can only view / read the state. The following statements are considered modifying the state:

## virtual & override
Base functions can be overridden by inheriting contracts to change their behavior if they are marked as `virtual`. The overriding function must then use the `override` keyword in the function header.

```
pragma solidity 0.8.13;

contract Base {
    function foo() virtual public returns (uint256) {
        return 13;
    }

    function bar() virtual public returns (uint256) {
        return 22;
    }
}

contract Inherited is Base {
    function foo() public pure override returns (uint256) {
        return 10;
    }

    function bar() public override returns (uint256) {
        return super.bar();
    }
}
```


### Writing to state variables.

```
pragma solidity 0.8.13;

contract Contract {
    address internal owner;
    uint256 asd = 123;

    function foo() public view {
        asd = 10; // Function cannot be declared as view because this expression (potentially) modifies the state.
    }
}
```

### Emitting events.

```
pragma solidity 0.8.13;

contract Contract {
    function foo() public view {
        emit E(); // Function cannot be declared as view because this expression (potentially) modifies the state.
    }

    event E();
}
```

### Creating other contracts.

```
pragma solidity 0.8.13;

contract Contract {
    function foo() public view {
        X x = new X(); // Function cannot be declared as view because this expression (potentially) modifies the state.
    }
}

contract X {}
```

### Using selfdestruct.

### Sending Ether via calls.

### Calling any function not marked view or pure.

### Using low-level calls.

### Using inline assembly that contains certain opcodes.

# Data Locations (other keywords)

*This is probably new stuff after solidity compiler version ^0.5* 

## memory

lifetime is limited to an external function call - variable exists while a function is being called

## storage

the location where the state variables are stored, where the lifetime is limited to the lifetime of a contract

## calldata

special data location that contains the function arguments

# Misc

## *internal call*
https://ethereum.stackexchange.com/questions/19380/external-vs-public-best-practices

Internal calls are executed via jumps in the code, and array arguments are passed internally by pointers to memory. Thus, when the compiler generates the code for an internal function, that function expects its arguments to be located in memory.
