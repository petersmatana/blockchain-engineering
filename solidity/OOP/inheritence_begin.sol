// SPDX-License-Identifier: GPL-3.0
pragma solidity 0.8.13;

/*

Theory:
When a contract inherits from other contracts,
only a single contract is created. All function
is compiled into single contract, so internall
calls are done with JUMP, not with message call.

*/

contract Owned {
    address payable owner;

    constructor() {
        owner = payable(msg.sender);
    }
}

// cost 130149 gas
contract Destructible is Owned {
    function destroy() virtual public {
        if (msg.sender == owner){
            selfdestruct(owner);
        }
    }
}

// cost 130149 gas
contract AllTogether {
    address payable owner;

    constructor() {
        owner = payable(msg.sender);
    }

    function destroy() virtual public {
        if (msg.sender == owner){
            selfdestruct(owner);
        }
    }
}
