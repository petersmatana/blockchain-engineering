// SPDX-License-Identifier: GPL-3.0
pragma solidity 0.8.14;

contract Mapping {

    address zeroAddress = 0x0000000000000000000000000000000000000000;

    mapping(address => Stake) accounts;

    struct Stake {
        address user;
        uint256 number;
    }

    event Message(address addr, uint256 number, string text);

    constructor() {
    }

    function _contains() internal view returns (bool memoty, Stake storage) {
        Stake storage tmp = accounts[msg.sender];
        if (tmp.user == zeroAddress) {
            return (false, tmp);
        } else {
            return (true, tmp);
        }
    }

    function addStruct() public returns (bool) {
        Stake memory stake;
        bool success;
        
        (success, stake) = _contains();

        if (! success) {
            accounts[msg.sender] = Stake(msg.sender, 13);
            return true;
        } else {
            return false;
        }
    }

    function getStruct() public returns (bool) {
        Stake memory stake;
        bool success;
        
        (success, stake) = _contains();

        if (success) {
            emit Message(stake.user, stake.number, "get successfuly data");
        }

        return success;
    }

    function deleteStruct() public returns (bool) {
        Stake memory stake;
        bool success;
        
        (success, stake) = _contains();

        if (success) {
            delete accounts[msg.sender];
            return true;
        } else {
            return false;
        }
    }
}

