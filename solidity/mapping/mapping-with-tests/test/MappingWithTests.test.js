const MyContract = artifacts.require("MappingWithTests");
contract('MyContract', (accounts) => {
    let contract = before(async () => {
        contract = await MyContract.deployed({
            from: accounts[0]
        });
    })

    it('add Struct', async () => {
        var result = await contract.addStruct({
            from: accounts[1]
        });

        result.receipt.logs.forEach(element => {
            assert.equal(element.args.success, true);
        });
    })

    it('same wallet would like to add but fail', async () => {
        try {
            var result = await contract.addStruct({
                from: accounts[1]
            });
        } catch (error) {
            assert.equal(error.reason, 'You are owner.');
        }
    })

    it('owner get data from Struct', async () => {
        var result = await contract.getStruct({
            from: accounts[1]
        });

        result.logs.forEach(element => {
            // console.log(element.0);
        })
    })
});
