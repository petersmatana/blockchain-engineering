// SPDX-License-Identifier: GPL-3.0
pragma solidity 0.8.13;

contract NestedMapping {

    /*
        0x123   -> 1 = true
                -> 2 = false
                -> 3 = false
        0x234   -> 2 = true
            ...
    */
    mapping(address => mapping(uint => bool)) public nested;

    function add(address _address, uint _number, bool _bool) public {
        nested[_address][_number] = _bool;
    }

    /*
        Mapping probably can not be return.
    */
    // function getByAddress() public returns (mapping(uint => bool) memory value) { }

    /*
        If address do not exist in nested structure it raised error.
        If _number do not exist in structure, it returs false.
    */
    function get(address _address, uint _number) public view returns (bool) {
        return nested[_address][_number];
    }

    // do not work as expected :(
    function getChecked(address _address, uint _number) public returns (bool) {
        if (abi.encodePacked(nested[_address][_number]).length > 0) {
            emit Message("address is 0x0..0");
        } else {
            emit Message("address is not 0x0..0");
        }

        return nested[_address][_number];
    }

    event Message(
        string text
    );
}
