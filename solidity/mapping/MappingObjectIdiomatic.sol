// SPDX-License-Identifier: GPL-3.0
pragma solidity 0.8.14;

contract Mapping2 {

    address zeroAddress = 0x0000000000000000000000000000000000000000;

    mapping(address => Stake) accounts;

    struct Stake {
        address user;
        uint256 number;
    }

    event Log(string log);
    event Message(address addr, uint256 number, string text);

    constructor() {
    }

    function _contains() internal view returns (bool success, Stake storage) {
        require(accounts[msg.sender].user != zeroAddress, "You are not owner.");
        return (true, accounts[msg.sender]);
    }

    function _doNotContains() internal view returns (bool success) {
        require(accounts[msg.sender].user == zeroAddress, "You are owner.");
        return true;
    }

    function addStruct() public returns (bool) {
        _doNotContains();
        accounts[msg.sender] = Stake(msg.sender, 13);

        return true;
    }

    function getStruct() public {
        Stake memory stake;
        bool success;
        
        (success, stake) = _contains();
        emit Message(stake.user, stake.number, "get successfuly data");
    }

    function deleteStruct() public {
        _contains();
        delete accounts[msg.sender];
    }

}
