// SPDX-License-Identifier: GPL-3.0
pragma solidity >=0.7.0 <0.9.0;

contract StructureOfAContract {

    uint256 uint256Var;
    bool boolVar;
    uint8 uint8Var;
    uint16 unit16Var;
    uint32 uint32Var;
    uint64 uint64Var;
    uint128 uint128Var;

    // If I change order, gas is still equal to 67066.

}
