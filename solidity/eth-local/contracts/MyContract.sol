// SPDX-License-Identifier: MIT
pragma solidity 0.8.14;

contract MyContract {
  // using Address for add;

  uint256 public oneDay = 1 days;
  uint256 public oneMonth = 31 days;

  function sumDays() public view returns (uint256) {
    return oneDay + oneMonth;
  }

  function helloWorld() public pure returns (uint256) {
    return 13;
  }
}
