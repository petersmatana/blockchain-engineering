// SPDX-License-Identifier: GPL-3.0
pragma solidity 0.8.13;

import "@openzeppelin/contracts/access/Ownable.sol";

/*
    Ownable contract bring 3 functions:
    - renounceOwnable
    - transferOwnable
    - owner
*/
contract OwnableContract is Ownable {

    /* remove owner so all functions from Ownable
        or with onlyOwner can't be call
        owner will be - 0x0000...000
    */
    function renounceOwnership() public virtual override {
        super.renounceOwnership();
    }

    /*
        this function transfer ownership into another account
    */
    function transferOwnership(address _address) public virtual override {
        super.transferOwnership(_address);
    }

    /*
        returns who is owner
    */
    function owner() public override view returns (address) {
        return super.owner();
    }

    /*
        I try to hack this contract and somehow assign new owner.
        this fill not work. renounceOwnership implements modifier
        onlyOwner which can't pass through address 0x0..0
    */
    // function customTransferOwnership(address _newOwner) public virtual {
    //     super.transferOwnership(_newOwner);
    // }

    // function accessOwnerVariable(address _address) external {
    //     // this will not work. I can have an storage address to
    //     // modify it.
    //     // this._owner = _address;
    // }
}
