// SPDX-License-Identifier: GPL-3.0
pragma solidity >=0.7.0 <0.9.0;

// https://docs.soliditylang.org/en/v0.8.13/structure-of-a-contract.html
contract StructureOfAContract {

    // This is state variable.
    // Values of state variable are pernamently stored in contract storage.
    uint storeData;

    address myAddress;

    // Modifier need to have in the end this: _;
    modifier thisIsModifier() {
        emit Message("this is modifier");
        _;
    }

    // First of all is called thisIsModifier() and after
    // that body of function toBeOverride(). 
    function toBeOverride() public thisIsModifier {
        emit Message("to be override");
    }

    // Why this function need to be pure?
    function raiseError() pure public  {
        revert ThisIsError("this is error");
    }

    error ThisIsError(
        string message
    );

    event Message(
        string message
    );

    struct Structure {
        uint number;
        address owner;
    }

    enum Enumeration {
        Created,
        Locked,
        Inactive
    }

    function addressMembers() public {
        emit Message(addressMembers.transfer(msg.sender));
    }

}

// Helper function is defined outside of contract.
// Why? And what is purpose to define func outside?
function helper() pure {

}
