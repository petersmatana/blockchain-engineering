// SPDX-License-Identifier: GPL-3.0
pragma solidity >=0.7.0 <0.9.0;

contract Comments {

    // NatSpec is something like special types of comments.

    // This is regular singel line comment.
    /// This is NetSpec single line comment.

    /*
        This is regular multiple line comment.
    */

    /**
        This is NetSpec multi line comment.
    */

}

/// @title A title that should describe the contract/interface. Context: contract, interface.
/// @author The name of the author. Context: contract, interface, function.
/// @notice Explain to an end user what this does. Context: contract, interface, function.
/// @dev Explain to a developer any extra details. Context: contract, interface, function.
contract DoxygenTags {

    // Doxygen is documentation generator.

    /// @notice Example func.
    /// @param _number Documents a parameter just like in Doxygen (must be followed by parameter name). Context: function.
    /// @return Documents the return type of a contract’s function. Context: function.
    function func(uint256 _number) public returns (uint256) {

    }

    /**
        @notice Example func2 documented with multiple line comment.
        @param _number This is param.
        @return This is return.
    */
    function func2(uint256 _number) public returns (uint256) {

    }

}
