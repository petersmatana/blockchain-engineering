// SPDX-License-Identifier: GPL-3.0
pragma solidity 0.8.13;

contract PayableExample {

    /*
        Payable address can receive Ether. Can address
        also send Ether?
    */
    address payable public owner;

    address constant private developerAddress = 0xdD870fA1b7C4700F2BD7f44238821C26f7392148;

    /*
        Payable constructor can receive Ether. And
        what about send?

        Here is the case: developer of contract would like
        to receive fee from deplyed contract. So if contract
        is deployed, constructor should handle this payment.
    */
    constructor() payable {
        owner = payable(msg.sender);
    }

    /*
        Function to deposit Ether into this contract.
        Call this function along with some Ether.
        The balance of this contract will be automatically updated.

        But sorry this do not make any sense.
    */
    function deposit() public payable { }

    /*
        Call this function along with some Ether.
        The function will throw an error since this function is not payable.
    */
    function notPayable() public {
    }

    /*
        Return actual balance of this contract.
    */
    function getBalance() public view returns (uint) {
        return address(this).balance;
    }
}
