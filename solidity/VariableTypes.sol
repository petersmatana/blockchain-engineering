// SPDX-License-Identifier: GPL-3.0
pragma solidity 0.8.13;

/*
    Local: declared inside a fucntion. Not stored on the blockchain.
    State: declared outside a function. Stored on the blockchain.
    Global: provided information about the blockchain.

    Constant: can not be modified.
    Immutable: can be set via contructor and after that is immutable.
*/
contract VariablesTypes {

    // state variables    
    string public text = "hello";
    uint public number = 22;

    // constant
    uint public constant number2 = 13;

    // immutable
    uint public immutable number3;

    constructor() {
        number3 = 123;
    }

    // need to send a transaction to write to a state variable
    function setNumber(uint _number) public {
        number = _number;
    }

    // reading from state variable not need send transaction
    // so it do not need any gas
    function getNumber() public view returns (uint) {
        return number;
    }

    function doSomething() public {
        // local variable
        uint i = 13;

        emit Message1("local variable", i);

        // global variables
        uint timestamp = block.timestamp;
        address sender = msg.sender;

        emit Message1("global variable", timestamp);
        emit Message2("global variable", sender);
    }

    event Message1(
        string message,
        uint number
    );

    event Message2(
        string message,
        address addr
    );

}
