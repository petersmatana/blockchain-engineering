// SPDX-License-Identifier: GPL-3.0
pragma solidity 0.8.13;

// https://docs.soliditylang.org/en/v0.8.12/units-and-global-variables.html#meta-type

import "hardhat/console.sol";

contract C {
}

interface I {
    function foo() external returns (uint256);
}

contract Observe {
    function getName() public view {
        console.log(type(C).name);
    }

    function getInterfaceId() public pure returns (bytes4) {
        return type(I).interfaceId;
    }
}

