// SPDX-License-Identifier: GPL-3.0
// remix.ethereum.org/
pragma solidity >=0.7.0 <0.9.0;

contract Coin {

    address public minter;
    mapping (address => uint) public balances;

    constructor() {
        emit Logger("muj constructor");

        minter = msg.sender;
    }

    function mint(address receiver, uint amount) public {
        emit Logger("mint");

        // tomuhle uplne nerozumim a if ocividne funguje
        // require(msg.sender == minter);
        if (msg.sender == minter) {
            balances[receiver] += amount;
            emit Logger("mint go fine");
        } else {
            emit Logger("sender and minter address have to be the same");
            return;
        }
    }

    function send(address receiver, uint amount) public {
        emit Logger("send");

        if (amount > balances[msg.sender]) {
            emit Logger("insufficient balance");
            return;
        } else {
            balances[msg.sender] -= amount;
            balances[receiver] += amount;

            emit Sent(msg.sender, receiver, amount);
            emit Logger("send sucessfuly");
        }
    }

    // zatim nevim jak to napsat, chci vratit balance na nejakem uctu
    // function getBalance(address add) public uint {
    //     emit Logger("get balance");
    //     return balances[add];
    // }

    function log() public {
        emit Logger("hello from logger");
    }

    event Sent(
        address from,
        address to,
        uint amount
    );

    event Logger(
        string value
    );

}
