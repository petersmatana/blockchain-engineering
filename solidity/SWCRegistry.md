# Smart Contract Weakness Classification - SWC Registry

## SWC-100 - Function Default Visibility

https://swcregistry.io/docs/SWC-100

Functions that do not have a function visibility type specified are `public` by default. This can lead to a vulnerability if a developer forgot to set the visibility and a malicious user is able to make unauthorized or unintended state changes.

## SWC-101 - Integer Overflow and Underflow

https://swcregistry.io/docs/SWC-101

An overflow/underflow happens when an arithmetic operation reaches the maximum or minimum size of a type.

```
pragma solidity >=0.7.0 <0.9.0;

contract X {

  function f() public pure returns(uint8) {
    uint8 x = 255;
    x = x + 2;
    return x; // returns 0
  }
  
  function f2() public pure returns(uint8) {
    uint8 x = 255;
    x = x + 10;
    return x; // returns 0
  }

}
```

## SWC-102 - Outdated Compiler Version

https://swcregistry.io/docs/SWC-102

Using an outdated compiler version can be problematic especially if there are publicly disclosed bugs and issues that affect the current compiler version.

## SWC-103 - Floating Pragma

https://swcregistry.io/docs/SWC-103

According to SWC is good have fixed compiler version.

### Floating Pragma
```
pragma solidity ^0.4.0;

contract PragmaNotLocked {
    uint public x = 1;
}
```

### Floating Pragma Fixed
```
pragma solidity 0.4.25;

contract PragmaFixed {
    uint public x = 1;
}
```

## SWC-104 - Unchecked Call Return Value

Best practice is not to use call().

## SWC-105 - Unprotected Ether Withdrawal

## SWC-106 - Unprotected SELFDESTRUCT Instruction

https://swcregistry.io/docs/SWC-106

This hack is about multi-signature wallets. Two or more wallets is need to approve something like withdraw or setup admin.
There was some library which implement multi-signature wallets. In a simple way - constructor of this library was replaced with malicious one where attacer take ownership of this contract. After that attacker call kill function which make that all wallets which has some funds in smart contract became useless. 

### Lessons learned
- It is very good practice to NOT have kill function to kill smart contract. When attacker take ownership of the contract without kill function, attacker wouldn't be able to do anything.
- Properly checked how initialization is going.
- *Be sure if included library do not allowing some externall call which can be replaced with malicious functionality.*

## SWC-107 - Reentrancy

https://swcregistry.io/docs/SWC-107

Calling external smart contract can take over controll flow of my own smart contract. In the reentrancy attack (a.k.a. recursive call attack), a malicious contract calls back into the calling contract before the first invocation of the function is finished. This may cause the different invocations of the function to interact in undesirable ways.

// TODO naucit se co je call()

## SWC-108 - State Variable Default Visibility

https://swcregistry.io/docs/SWC-108

It is highly recommended to declare variables with keywords `public` or `internal` or `private`.

## SWC-109 - Uninitialized Storage Pointer

https://swcregistry.io/docs/SWC-109

Uninitialized local storage variables can point to unexpected storage locations in the contract, which can lead to intentional or unintentional vulnerabilities.

This issue is solved in higher compilers, 0.5.0 and above.

## SWC-110 - Assert Violation

https://swcregistry.io/docs/SWC-110

The Solidity `assert()` function is meant to assert invariants. Invariant is something like state which is **always** true. Properly functioning code should never reach a failing assert statement. If assert can reach state which is false it is recommended to use `require()`.

## SWC-111 - Use of Deprecated Solidity Functions

https://swcregistry.io/docs/SWC-111

Several functions and operators in Solidity are deprecated. Using them leads to reduced code quality. With new major versions of the Solidity compiler, deprecated functions and operators may result in side effects and compile errors.

|Deprecated|Alternative|
|--|--|
|`suicide(address)`|`selfdestruct(address)`|
|`block.blockhash(uint)`|`blockhash(uint)`|
|`sha3(...)`|`keccak256(...)`|
|`callcode(...)`|`delegatecall(...)`|
|`throw`|`revert()`|
|`msg.gas`|`gasleft`|
|`constant`|`view`|
|`var`|corresponding type name|

## SWC-112 - Delegatecall to Untrusted Callee

https://swcregistry.io/docs/SWC-112

There exists a special variant of a message call, named delegatecall which is identical to a message call apart from the fact that the code at the target address is executed in the context of the calling contract and msg.sender and msg.value do not change their values. This allows a smart contract to dynamically load code from a different address at runtime. Storage, current address and balance still refer to the calling contract.

Calling into untrusted contracts is very dangerous, as the code at the target address can change any storage values of the caller and has full control over the caller's balance.

## SWC-113 - DoS with Failed Call

https://swcregistry.io/docs/SWC-113

External calls can fail accidentally or deliberately, which can cause a DoS condition in the contract. To minimize the damage caused by such failures, it is better to isolate each external call into its own transaction that can be initiated by the recipient of the call. This is especially relevant for payments, where it is better to let users withdraw funds rather than push funds to them automatically (this also reduces the chance of problems with the gas limit).
Remediation

### Lessons learned
- Avoid combining multiple calls in a single transaction, especially when calls are executed as part of a loop
- Always assume that external calls can fail
- Implement the contract logic to handle failed calls

## SWC-114 - Transaction Order Dependence

https://swcregistry.io/docs/SWC-114

For example: transaction-ordering attack will change price during the processing of my transaction because some one else sent transaction modifying price before my transaction is complete.

## SWC-115 - Authorization through tx.origin

https://swcregistry.io/docs/SWC-115

### Lessons learned
Recomendiation is that `tx.origin` should not be used for authorization. Use `msg.sender` instead.

## SWC-116 - Block values as a proxy for time

https://swcregistry.io/docs/SWC-116

### Lessons learned
Developers should write smart contracts with the notion that block values are not precise, and the use of them can lead to unexpected effects. Alternatively, they may make use oracles - Oracle pattern.

## SWC-117 - Signature Malleability

https://swcregistry.io/docs/SWC-117

### Lessons learned
A signature should never be included into a signed message hash to check if previously messages have been processed by the contract.

## SWC-118 - Incorrect Constructor Name

https://swcregistry.io/docs/SWC-118

Just it's better to use new version of compiler.

## SWC-119 - Shadowing State Variables

https://swcregistry.io/docs/SWC-119

Just avoid ambiguities variable naming.

// learn what is happening with inheritance

## SWC-120 - Weak Sources of Randomness from Chain Attributes

https://swcregistry.io/docs/SWC-120

Creating a strong enough source of randomness in Ethereum is very challenging.

### Lessons learned
- Using commitment scheme, e.g. RANDAO.
- Using external sources of randomness via oracles, e.g. Oraclize. Note that this approach requires trusting in oracle, thus it may be reasonable to use multiple oracles.
- Using Bitcoin block hashes, as they are more expensive to mine.

## SWC-121 - Missing Protection against Signature Replay Attacks

https://swcregistry.io/docs/SWC-121

Sometime is necessary to perform signature verification to achieve better usability or to save gas cost. A secure implementation needs to protect against Signature Replay Attacks by for example keeping track of all processed message hashes and only allowing new message hashes to be processed. A malicious user could attack a contract without such a control and get message hash that was sent by another user processed multiple times.

### Lessons learned
In order to protect against signature replay attacks consider the following recommendations:
- Store every message hash that has been processed by the smart contract. When new messages are received check against the already existing ones and only proceed with the business logic if it's a new message hash.
- Include the address of the contract that processes the message. This ensures that the message can only be used in a single contract.
- Under no circumstances generate the message hash including the signature. The `ecrecover` function is susceptible to signature malleability (see also SWC-117).

## SWC-122 - Lack of Proper Signature Verification

https://swcregistry.io/docs/SWC-122

### Lessons learned
It is not recommended to use alternate verification schemes that do not require proper signature verification through `ecrecover()`.

## SWC-123 - Requirement Violation

https://swcregistry.io/docs/SWC-123

Just have tests on input validation? This issue is about what happening If validation requirement is too strong.

## SWC-124 - Write to Arbitrary Storage Location

https://swcregistry.io/docs/SWC-124

Attacker can write to sensitive storage location. If this can happend, attacker can authorization check can be omitted.

## SWC-125 - Incorrect Inheritance Order

https://swcregistry.io/docs/SWC-125

Contract order in inheritance matters. Potential source of attacks.

## SWC-126 - Insufficient Gas Griefing

https://swcregistry.io/docs/SWC-126

Attack through insufficient of gas.

### Lessons learned
- Only allow trusted users to relay transactions.
- Require that the forwarder provides enough gas.

## SWC-127 - Arbitrary Jump with Function Type Variable

https://swcregistry.io/docs/SWC-127

The use of assembly should be minimal.

## SWC-128 - DoS With Block Gas Limit

https://swcregistry.io/docs/SWC-128

Programming patterns that are harmless in centralized applications can lead to Denial of Service conditions in smart contracts when the cost of executing a function exceeds the block gas limit. Modifying an array of unknown size, that increases in size over time, can lead to such a Denial of Service condition.

### Lessons learned
Do not loop over array of unknown size.

If you absolutely must loop over an array of unknown size, then you should plan for it to potentially take multiple blocks, and therefore require multiple transactions.

## SWC-129 - Typographical Error

https://swcregistry.io/docs/SWC-129

Bad usage of += and =+.

## SWC-130 - Right-To-Left-Override control character

https://swcregistry.io/docs/SWC-130

Character U+202E -- Right-To-Left-Override should not appear in the source code.

## SWC-131 - Presence of unused variables

https://swcregistry.io/docs/SWC-131

Do not use unused variables.

## SWC-132 - Unexpected Ether balance

https://swcregistry.io/docs/SWC-132

Contracts can behave erroneously when they strictly assume a specific Ether balance. In the worst scenario it can lead to DoS attact.

### Lessons learned
Avoid strict equality checks for the Ether balance in a contract.

## SWC-133 - Hash Collisions With Multiple Variable Length Arguments

https://swcregistry.io/docs/SWC-133

### Lessons learned
When using `abi.encodePacked()`, it's crucial to ensure that a matching signature cannot be achieved using different parameters. To do so, either do not allow users access to parameters used in `abi.encodePacked()`, or use fixed length arrays. Alternatively, you can simply use `abi.encode()` instead.

## SWC-134 - Message call with hardcoded gas amount

https://swcregistry.io/docs/SWC-134

### Lessons learned
Avoid the use of `transfer()` and `send()` and do not otherwise specify a fixed amount of gas when performing calls. Use `.call.value(...)("")` instead. Use the checks-effects-interactions pattern and/or reentrancy locks to prevent reentrancy attacks.

## SWC-135 - Code With No Effects

https://swcregistry.io/docs/SWC-135

In Solidity, it's possible to write code that does not produce the intended effects. Currently, the solidity compiler will not return a warning for effect-free code. This can lead to the introduction of "dead" code that does not properly performing an intended action.

For example, it's easy to miss the trailing parentheses in `msg.sender.call.value(address(this).balance)("");`, which could lead to a function proceeding without transferring funds to `msg.sender`. Although, this should be avoided by checking the return value of the call.

## SWC-136 - Unencrypted Private Data On-Chain

https://swcregistry.io/docs/SWC-136

It is a common misconception that `private` type variables cannot be read. Even if your contract is not published, attackers can look at contract transactions to determine values stored in the state of the contract. For this reason, it's important that unencrypted private data is not stored in the contract code or state.

### Lessons learned
Any private data should either be stored off-chain, or carefully encrypted.
