// SPDX-License-Identifier: GPL-3.0
pragma solidity 0.8.13;

contract Enum {

    enum Status {
        Begin,
        InProgress,
        Finished
    }

    Status public status;

    /*
        This should return uint.
        Begin - 0
        InProgress - 1
        Finished - 2
    */
    function getStatus() public view returns (Status) {
        return status;
    }

    // This accept uint
    function setStatus(Status _status) public {
        status = _status;
    }

    function setFinish() public {
        status = Status.Finished;
    }

}
