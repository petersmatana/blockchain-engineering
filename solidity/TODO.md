### Solidity vulnerabilities

https://github.com/petr-hejda/solidity-vulnerabilities

# ERCs

ERC-721
ERC-223
ERC-621
ERC-777
ERC-827
ERC-884
ERC-865
ERC-1155

# Examples

https://solidity-by-example.org/

# Od Alexe

Za me bych doporucil ponorit se trochu hloubeji i do vnitrku fungovani etherea:
- struktura transakci, digitalni podpisy
- JSON-RPC interface, pres ktery se komunukuje s eth nodem (pres ktery vlastne na blokchain pristupujes)
- jak se pocita gas
- migrace proof of work -> proof of stake
- co obnasi spustit vlastni node
Dale:
- solidity eventy, event subscription
- Oracles, interakce s datovymi zdroji mimo blockchain
- projit vsechny ERC standardy a aspon zhruba vedet, o cem jsou a jaky problem resi
- udelat si prehled o L2 sitich, pripadne uplne alternativnich blockchainech (Solana, Polygon, Avalanche, Polkdadot, BSC atd)

# Moje

- jak kontrolovat, jestli v mapping strukture podle klice existuje nejaka hodnota
- abi.encodePacked
- celkem easy https://solidity-by-example.org/function ale nechapu co je destructing assignment
