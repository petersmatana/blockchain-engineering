// SPDX-License-Identifier: GPL-3.0
pragma solidity >=0.7.0 <0.9.0;

// Owner can deposit and withdraw anything.
// Owner can change settings.
// Other people cat deposit and withdraw only configured amount.

contract MyRealExample {

    // Address of the owner.
    address owner;

    // Amount value which can be deposit.
    uint deposit;

    // Amoutn value whic can be withdraw.
    uint withdraw;

    // Whole account balance.
    uint accountBalance;

    // evidence of all who want to deposit or withdraw
    mapping (address => uint) balanceEvidence;

    constructor() {
        owner = msg.sender;
        deposit = 5;
        withdraw = 5;
    }

    function Deposit() public payable {
        if (owner == msg.sender) {
            // This is owner.
            accountBalance += msg.value;
        } else {
            // This is not owner.

            if (msg.value != deposit) {
                emit Message("Deposit have to be exact amount");
                return;
            } else {
                accountBalance += msg.value;
                balanceEvidence[msg.sender] += msg.value;
            }
        }
    }

    function Withdraw(uint _amount) public payable {
        if (owner == msg.sender) {
            // This is owner.
            if (accountBalance < _amount) {
                emit Message("Hello owner. There is no enough funds.");
            } else {
                accountBalance -= _amount;
                payable(owner).transfer(_amount);
            }
        } else {
            // This is not owner.

            if (balanceEvidence[msg.sender] < withdraw) {
                emit Message("You have no enough funds.");
            } else {
                balanceEvidence[msg.sender] -= withdraw;
                accountBalance -= withdraw;
                payable(msg.sender).transfer(withdraw);
            }
        }
    }

    function setDepositLimit(uint _amount) public returns (uint) {
        requiredOwner();
        deposit = _amount;
        return deposit;
    }

    function getDepositLimit() public view returns (uint) {
        return deposit;
    }

    function setWithdrawLimit(uint _amount) public returns (uint) {
        requiredOwner();
        withdraw = _amount;
        return withdraw;
    }

    function getWithdrawLimit() public view returns (uint) {
        return withdraw;
    }

    function requiredOwner() view private {
        require(owner == msg.sender, "You are not the owner of this Smart Contract.");
    }

    function getBalance() public view returns (uint) {
        return accountBalance;
    }

    event Message(
        string value
    );

}
