supply = dodávka (zásobení)
demand = poptávka
store of value = uchovatel hodnoty
tangible value = hmatatelná hodnota
non-collateralized = nezajištěné
fee = odměna
rates = sazby
share = podíl
flat fee = nájemné
ledger = účetní kniha
peer = druh / osoba stejného stavu
lack of interoperability = nedostatek interoperability - schopnost různých systémů vzájemně spolupracovat
opacity = neprůhlednost
insured = pojistěný
deposits = vklad
interest = podíl
remittance fees = poplatky za převod
settle a bill = vyrovnat účet
transfer of funds = přenos finančních prostředků
brokerage = makléřství
assets
cleaning house

over-crypto-fiat-non collateralized
hard-soft pegged, peg
seigniorage model
caveat
divisible
custodians
stipulate = stanovit
pro rata = poměrným dílem
perpetuity
assets
compound
catchall bucket
redemption = vykoupení
inflation / deflation supply
unitary assets
custody = péče
escrow = podmíněné
intentionally = záměrně
flip = převrátit
bonding = lepení
monotonically = monotoní
uncapped
incentives = pobídky
collateral = vedlejší

considerable = nemalý / velký
egregious = křiklavý
implausible = nepravděpodobný
interoperability
incentives = pobídky
cumberstone

incentive distribution
