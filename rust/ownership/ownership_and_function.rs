fn main() {
    let s1 = fu_give_ownership();
    println!("ownership of s1 in main = {}", s1);

    let mut s2 = String::from("🦀");

    println!("here in main is ownership of s2 = {}", s2);
    s2 = fn_take_and_give_back_ownership(s2);
    println!("again print s2 = {}", s2);

    let len = change_string_with_reference(&s2);
    println!("len of s2 = {}", len);
}

fn fu_give_ownership() -> String {
    let s1 = String::from("🎵");
    println!("ownership in fn = {}", s1);
    return s1;
}

fn fn_take_and_give_back_ownership(s: String) -> String {
    // This is of course very bad design.
    let mut tmp = s;

    println!("here in fn is ownership of string = {}", tmp);
    tmp.push_str(" - some suffix");

    return tmp;
}

fn change_string_with_reference(s: &String) -> usize {
    return s.len();
}