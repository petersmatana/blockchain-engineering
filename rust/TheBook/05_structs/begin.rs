struct User {
    active: bool,
    username: String,
    email: String,
    sign_in_count: u64
}

fn main() {
    immutable();

    println!("");

    mutable();

    println!("");

    struct_update_syntax();
}

fn immutable() {
    let user = user_builder(String::from("smonty"), String::from("email@smonty.cz"));

    print_user(user);
}

fn mutable() {
    let mut user = user_builder(String::from("smonty"), String::from("email@smonty.cz"));

    user.sign_in_count = 22;

    print_user(user);
}

// probably official name is: struct update syntax
fn struct_update_syntax() {
    let user1 = user_builder(String::from("Tom"), String::from("Tom@disney.com"));

    println!("");
    print_user(user1);

    let user2 = User {
        email: String::from("another@email.com"),
        ..user1
    };
    print_user(user2);
}

fn user_builder(username: String, email: String) -> User {
    User {
        active: true,
        username,
        email,
        sign_in_count: 1,
    }
}

fn print_user(user: User) {
    println!("username: {}\nemail: {}\nactive: {}\nsign in count: {}",
        user.username,
        user.email,
        user.active,
        user.sign_in_count
    );
}
