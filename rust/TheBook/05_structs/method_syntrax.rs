struct Rectangle {
    width: u32,
    height: u32,
}

impl Rectangle {
    fn area(&self) -> u32 {
        self.width * self.height
    }

    fn is_square(&self) -> bool {
        self.width == self.height
    }

    fn fit_inside(&self, other: &Rectangle) -> bool {
        self.width > other.width && self.height > other.height
    }

    fn is_bigger(&self, other: &Rectangle) -> bool {
        self.area() > other.area()
    }
}

fn main() {
    let rect1 = Rectangle {
        width: 10,
        height: 20,
    };

    let rect2 = Rectangle {
        width: 15,
        height: 15,
    };

    println!("area: {}", rect1.area());

    if rect1.is_square() {
        println!("rectangle is square");
    } else {
        println!("rectangle is not square");
    }

    println!("fit inside {}", rect1.fit_inside(&rect2));
    println!("is bigger {}", rect1.is_bigger(&rect2));
}
