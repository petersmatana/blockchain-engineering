#[allow(dead_code)]
enum Option<T> {
    None,
    Some(T),
}

fn main() {
    // let some_number = Some(5);
    // let some_string = Some("asd");

    // println!("{} {}", some_number, some_string)

    let absent_number: Option<i32> = None;
}
