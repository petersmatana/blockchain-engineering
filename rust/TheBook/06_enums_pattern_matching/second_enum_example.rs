#[allow(dead_code)]
struct QuitMessage; // unit struct

#[allow(dead_code)]
struct MoveMessage {
    x: i32,
    y: i32,
}

#[allow(dead_code)]
struct WriteMessage(String); // tuple struct

#[allow(dead_code)]
struct ChangeColorMessage(i32, i32, i32); // tuple struct

#[allow(dead_code)]
enum Message {
    Quit,
    Move { x: i32, y: i32 },
    Write(String),
    ChangeColor(i32, i32, i32),
}

impl Message {
    #[allow(dead_code)]
    fn print_data(&self) {
        // println!("{}", self.Write);
    }
}

fn main() {
    let _ = Message::Write(String::from("asd"));
}
