#[allow(dead_code)]
enum WithoutType {
    A,
    B,
    C,
}

#[allow(dead_code)]
enum WithType {
    A(String),
    B(u32),
    C(bool),
}

fn main() {
    let _ = WithoutType::A;

    let _ = WithType::A(String::from("Rust"));
    let _ = WithType::B(123);
}
