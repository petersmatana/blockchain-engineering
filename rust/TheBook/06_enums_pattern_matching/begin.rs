#[allow(dead_code)]
enum IpAddrKind {
    V4,
    V6,
}

#[allow(dead_code)]
struct IpAddr {
    kind: IpAddrKind,
    address: String,
}

#[allow(dead_code)]
enum StructEnum {
    Struct1(S1),
    Struct2(S2),
}

#[allow(dead_code)]
struct S1 {
    message: String,
}

#[allow(dead_code)]
struct S2 {
    message: String,
}

fn main() {
    let _ = IpAddr {
        kind: IpAddrKind::V4,
        address: String::from("127.0.0.1"),
    };

    let _ = IpAddr {
        kind: IpAddrKind::V6,
        address: String::from("::1"),
    };

    let struct1 = S1 {
        message: String::from("txt"),
    };

    let _ = StructEnum::Struct1(struct1);
}
