fn main() {
    iterate1();
    iterate2();
}

fn get_largest(vec: Vec<u32>) -> u32 {
    let mut largest_number: u32 = 0;
    
    for item in vec {
        if item > largest_number {
            largest_number = item;
        }
    }

    return largest_number;
}

fn iterate2() {
    let number_list = vec![34, 50, 25, 100, 65];

    println!("largest number is: {}", get_largest(number_list));
    // println!("largest number is: {}", get_largest2(&number_list2));
}

fn iterate1() {
    let number_list = vec![34, 50, 25, 100, 65];

    let mut largest = 0;

    for item in number_list {
        // println!("item: {}", item);
        if item > largest {
            largest = item;
        }
    }

    println!("largest number is: {}", largest);
}