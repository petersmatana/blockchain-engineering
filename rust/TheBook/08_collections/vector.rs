fn print_vector(v: Vec<u32>) {
    println!("data in vector:");
    for item in v {
        println!("  item in vector: {}",item);
    }
}

fn main() {
    let mut v: Vec<u32> = Vec::new();
    let mut v2: Vec<u32> = vec![1,2,3];
    let mut v3: Vec<u32> = vec![];

    v.push(123);
    print_vector(v);

    v2.push(4);
    print_vector(v2);

    v3.push(333);
    v3.push(334);
    print_vector(v3);
}