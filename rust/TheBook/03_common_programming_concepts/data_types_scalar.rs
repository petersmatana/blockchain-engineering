fn main() {
    u_size();
    integer_overflow();
    primitives();
    print_type_of_variable();
}

fn u_size() {
    // based on architecture, variable is allocated on 32 or 64bit.
    // 2^64-1
    let my_usize: usize = usize::MAX;
    println!("{}", my_usize);

    let mut my_isize: isize = isize::MIN;
    println!("min size of isize type on my PC is: {}", my_isize);
    my_isize = isize::MAX;
    println!("max size of isize type on my PC is: {}", my_isize);
}

fn integer_overflow() {
    // let my_var: u8 = 256;
    let my_var: u8 = 255;
    println!("{}", my_var);
}

fn primitives() {
    let i8var: i8 = -1;
    let i16var: i16 = -1;
    let i32var: i32 = -1;
    let i64var: i64 = -1;
    let i128var: i128 = -1;

    println!("i8 = {} \ni16 = {} \ni32 = {} \ni64 = {} \ni128 = {}", i8var, i16var, i32var, i64var, i128var);

    println!();

    let u8var: u8 = 1;
    let u16var: u16 = 1;
    let u32var: u32 = 1;
    let u64var: u64 = 1;
    let u128var: u128 = 1;

    println!("u8 = {} \nu16 = {} \nu32 = {} \nu64 = {} \nu128 = {}", u8var, u16var, u32var, u64var, u128var);

    println!();

    let f32var: f32 = 1.0;
    let f64var: f64 = 1.0;
    println!("f32 = {}\nf64 = {}", f32var, f64var);

    println!();

    let charvar: char = 'a';
    let boolvar: bool = true;
    println!("char = {}\nbool = {}", charvar, boolvar);
}

fn print_type_of<T>(_: &T) -> &'static str {
    std::any::type_name::<T>()
}

fn print_type_of_variable() {
    let v = vec![1,2,3];
    let v2: Vec<usize> = vec![1,2,3];

    println!("v: {:?} is {}", v, print_type_of(&v));
    println!("v2: {:?} is {}", v2, print_type_of(&v2));
}
