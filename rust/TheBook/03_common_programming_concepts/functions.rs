fn main() {
    f(13,'a');
    
    statement_example();
    get_expression(13);

    println!("1 + 1 = {}", addition(1,1));
    println!("1 + 1 = {}", addition2(1,1));
}

fn f(x: u8, ch: char) {
    println!("f args: {} and {}", x, ch);
}

// Statement = instruction which perform some action but not return a value.
// Expression = evaluate itself and return a value.

fn statement_example() {
    let mut x = 13;
    x = x + 1;
    println!("{}", x);

    // I can NOT make this:
    // let a = b = 22;
}

fn return_expression(number: i64) -> bool {
    if number > 22 {
        // This is the last expression in fn so it evaluates and returns.
        // So it is without semicolon
        true
    } else {
        // explicit return
        return false;
    }
}

fn get_expression(number: i64) {
    println!("result of expression is: {}", return_expression(number));
}

// expression is evaluated and returned without ;
fn addition(x: i64, y: i64) -> i64 {
    x + y
}

// expression is returned with keyword return
fn addition2(x: i64, y: i64) -> i64 {
    return x + y;
}
