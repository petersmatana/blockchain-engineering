const THREE_HOURS_IN_SECONDS: u32 = 60 * 60 * 3;

fn main() {
    immutable();
    mutable();
    constance();
    shadowing();
}

fn immutable() {
    let x = 13;
    println!("value of x is: {}", x);

    // can not do this because x is immutable
    // x = 22;
}

fn mutable() {
    let mut x = 13;
    println!("value of x is: {}", x);

    x = 22;
    println!("value of x is: {}", x);
}

fn constance() {
    println!("3 hours in seconds is: {}", THREE_HOURS_IN_SECONDS);
}

fn shadowing() {
    // this first variable is shadowed by second one 
    let x = 5;

    // but original value is taken and make +1
    let x = x + 1;

    {
        let x = x * 2;
        println!("The value of x in the inner scope is: {}", x);
    }

    println!("The value of x is: {}", x);
}
