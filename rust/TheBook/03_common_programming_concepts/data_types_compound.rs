fn main() {
    let tuplevar = (1,2,3,4);
    let arrayvar = [1,2,3,4];
    println!("tuple = {:?}\narray = {:?}", tuplevar, arrayvar);
    println!("first tuple element = {}", tuplevar.0);

    println!();

    let origin_tuple: (i32, bool) = (123, true);
    let x = reverse_tuple(origin_tuple);
    println!("bool = {}, number = {}", x.0, x.1);

    println!();

    play_with_tuples();
}

fn play_with_tuples() {
    let long_tuple: (i32, (u32, bool), (char, i32)) = (1, (1, true), ('a', 1));

    println!("whole tuple = {:?}", long_tuple);
    println!("bool = {}", long_tuple.1.1);
    println!("first tuple = {:?}", long_tuple.1);
    println!("second tuple = {:?}", long_tuple.2);
}

fn reverse_tuple(pair: (i32, bool)) -> (bool, i32) {
    (pair.1, pair.0)
}
