# Intro
Smart contract on Solana is called program.

Solana offers:
- 50k transactions/sec but sometime there are troubles...
- Nakamoto coefficient: 25 (for this moment)
    It is how many biggest pools have to join forces and try to control the network.
- $10 for 1 million transactions (for this moment)

## Solana VS. Ethereum
||Solana|Ethereum|
|--|--|--|
|proof|Proof of Stake with Proof of History|Proof of Work|
|block time|400ms|13s|
||Stateless|Stateful|
|language|Rust|Solidity|
|throughput|~50k TPS|15TPS|

## Why is Solana faster than Eth?
At single time one transaction upon Ethereum can happened. But in Solana transaction can happened in parallel. How this can be done? In Ethereum data and functions are encapsulated together. Solana split this so data and functions are loosely coupled.

## Format of Solana transaction

| 32 bytes | |

| signatures | message |

Message format:

| header | account addresses (signed, not signed, write, read) | block hash | instructions |

Instruction format:

| program-id (unique identifier) | accounts | data (what is sending to chain) |

## Basics
Client (CLI, Rust SDK, JavaScript SDK) is requesting the blockchain (Solana). This is done with 2 ways - with transaction or with query. This is done with JSON RPC API - making remote procedure call. But from helicopter point of view it is POST Request od network, content type is JSON and in the body are parameters like `"jsonrpc": "2.0"`, `method` or `params` which are passed into the method.
Another way is transactions. Transaction can be store state on chain or can modify state on blockchain.

## Hands on

`solana-test-validator -r` - local validator of transactions. `-r` is remove previous state of running. without -r it continue on the ledger of previous run. It do not have RPC limit, airdrops limits.

`solana config get` - useful info where is the config file, WebSocket & RPC URL or where is keypair. If there is no keypair set up, need to generate one with `solana-keygen new -o demo.json` (-o is where is the key stored). After generate new key it is need to run `solana config set -k /path/to/key/demo.json`

`solana airdrop 10 <account>` this command will add 10 SOL to account.

# Run example program from Ackee

I can not run `cargo build-bpf --manifest-path=Cargo.toml --bpf-out-dir=dist/program`. Probably I do not have `rustup`?
