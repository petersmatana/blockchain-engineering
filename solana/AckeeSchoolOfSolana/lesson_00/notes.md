1. Install Rust on your PC

https://www.rust-lang.org/tools/install

2. Install Solana tool suite

https://docs.solana.com/cli/install-solana-cli-tools

I am not 100% sure but solana-cli will install this apps:

```
solana
solana-bench-tps
solana-dos
solana-faucet
solana-genesis
solana-gossip
solana-install
solana-install-init
solana-keygen
solana-ledger-tool
solana-log-analyzer
solana-net-shaper
solana-stake-accounts
solana-sys-tuner
solana-test-validator
solana-tokens
solana-validator
solana-watchtower
```

3. Install Anchor framework

https://book.anchor-lang.com/getting_started/installation.html

docs: https://docs.rs/anchor-lang/latest/anchor_lang/

I did not install Anchor. Simple Anchor is a framework for quickly building secure Solana programs.

Anchor documentation probably wants to install `Yarn`? 

`cargo install --git https://github.com/project-serum/anchor avm --locked` or add option `--force`

but it is need to install openssl like `apt install libssl-dev`

After successful installation it is need to run: `avm use latest` or `avm install 0.24.2`

And after all of this `anchor` is ready to run.

4. Install Rust Analyzer

https://marketplace.visualstudio.com/items?itemName=rust-lang.rust-analyzer

Simple install in VSCode.

----

### Install troubles

Question:

Hello everyone. I am struggling whit this one. OpenSSL and other packages like pkg-config build-essential libudev-dev install. Running on Ubuntu 22.04. Thanks for any help.

```
smonty@smontypc$ openssl version
OpenSSL 3.0.2 15 Mar 2022 (Library: OpenSSL 3.0.2 15 Mar 2022)
smonty@smontypc$ cargo-build-bpf --manifest-path=Cargo.toml --bpf-out-dir=dist/program
BPF SDK: /home/smonty/.local/share/solana/install/releases/1.10.8/solana-release/bin/sdk/bpf
cargo-build-bpf child: rustup toolchain list -v
cargo-build-bpf child: cargo +bpf build --target bpfel-unknown-unknown --release
/home/smonty/.rustup/toolchains/bpf/bin/cargo: error while loading shared libraries: libssl.so.1.1: cannot open shared object file: No such file or directory
```

Answer:

OpenSSL 1.1.1 is not available on Ubuntu 22.04. You can install a version meant for Ubuntu 21 (don't worry, it should work fine on 22) manually using

```
cd ~/Downloads
wget http://archive.ubuntu.com/ubuntu/pool/main/o/openssl/libssl1.1_1.1.1l-1ubuntu1.4_amd64.deb
sudo dpkg -i ./libssl1.1_1.1.1l-1ubuntu1.4_amd64.deb
rm libssl1.1_1.1.1l-1ubuntu1.4_amd64.deb
```

What I do:

```
apt purge libssl-dev
```

And help from discord just work :D +1
