#[allow(dead_code)]
struct Person {
    first_name: String,
    last_name: String,
}

impl Person {

    #[allow(dead_code)]
    fn new(first: &str, second: &str) -> Person {
        Person {
            first_name: first.to_string(),
            last_name: second.to_string(),
        }
    }

    #[allow(dead_code)]
    fn name_length(&self) -> usize {
        self.first_name.len() + self.last_name.len()
    }
}

#[allow(dead_code)]
#[allow(unused_variables)]
pub fn basics() {
    let p: Person = Person::new("Tom", "Jones");

    println!("Person: {} {}", p.first_name, p.last_name);
    println!("Name length: {}", p.name_length());
}
