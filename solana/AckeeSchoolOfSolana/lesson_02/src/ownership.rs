#[allow(dead_code)]
pub fn drop_variable_1() {
    let v = vec![1,2,3];
    let v2 = v;

    // This will not work because v
    // is dropped
    // println!("vector: {:?}", v);
    println!("vector: {:?}", v2);
}

// ------------------------------------

#[allow(dead_code)]
pub fn drop_variable_2() {
    let v = vec![1,2,3];
    let v2: Vec<i32> = v.clone();

    println!("v: {:?} and v2: {:?}", v, v2);
}

// ------------------------------------

#[allow(dead_code)]
pub fn display(v: Vec<i32>) {
    println!("display print: {:?}", v);
}

#[allow(dead_code)]
pub fn lose_ownership() {
    let v: Vec<i32> = vec![1,2,3];

    display(v);
    // Can't do this because ownership
    // ends in display() fn.
    // println!("main print: {:?}", v);
}

// ------------------------------------

#[allow(dead_code)]
pub fn display_2(v: Vec<i32>) -> Vec<i32> {
    println!("display print: {:?}", v);
    v
}

#[allow(dead_code)]
pub fn get_ownership() {
    let mut v: Vec<i32> = vec![1,2,3];

    v = display_2(v);
    // I'm not 100% sure but here I send data
    // to fn and obtain new copy of data which
    // need more memory and lets say more CPU
    // time.
    println!("main print: {:?}", v);
}

// ------------------------------------

#[allow(dead_code)]
pub fn display_3(v: &Vec<i32>) {
    println!("display print: {:?}", v);
}

#[allow(dead_code)]
pub fn get_ownership_2() {
    let v: Vec<i32> = vec![1,2,3];

    // Here I pass only pointer not the
    // value so v whole time own the vec
    display_3(&v);
    println!("main print: {:?}", v);
}

// ------------------------------------

/*
    This could be something like he is
    talking about. 
*/

#[allow(dead_code)]
struct Msg{
    text: String
}

// #[allow(dead_code)]
// pub fn drop_ownership(input: &Text) {
//     println!("{}", input);
// }

#[allow(dead_code)]
fn create_msg() {
    // let msg1: &str = "msg 1";
    // Msg(msg1);

    let _ = Msg {
        text: String::from("msg 1"),
    };
}

#[allow(dead_code)]
pub fn fn_drop_ownership() {
    create_msg();
}

