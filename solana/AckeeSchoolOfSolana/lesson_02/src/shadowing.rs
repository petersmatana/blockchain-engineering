#[allow(dead_code)]
pub fn shadow() {
    let variable: &str = "this is string";
    println!("value of variable: {}", variable);

    let variable: usize = variable.len();
    println!("value of variable: {}", variable);
}