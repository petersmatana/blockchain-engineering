#[allow(dead_code)]
pub fn string_literal() {
    let hello: &str = "hello";
    let from: &str = "from";
    let rust: &str = "Rust";

    println!("{} {} {}", hello, from, rust);
}

#[allow(dead_code)]
pub fn string_object() {
    let empty_string = String::new();
    println!("length of empty string: {}", empty_string.len());
}