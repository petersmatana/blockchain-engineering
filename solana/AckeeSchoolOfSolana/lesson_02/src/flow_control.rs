#[allow(dead_code)]
#[allow(unused_variables)]
pub fn no_ternary_operator() {
    // Rust do not have ternary operator like C have
    let condition: bool = true;

    // and do not know why I can't do it like this:
    // let result: i32 = if condition { return 1; } else { return 2; };

    // println!("result is: {}", result);
}

#[allow(dead_code)]
pub fn no_ternary_operator2() {
    let condition: bool = true;
    let result: i32 = if condition { 1 } else { 2 };

    println!("result is: {}", result);
}
