/*
    this is used with cargo, so try:
    cargo build or cargo run
    in directory where Cargo.toml is
    located
*/

mod other_file;
mod string;
mod shadowing;
mod functions;
mod flow_control;
mod ownership;
mod slices;
mod oop;
mod mytrait;

fn main() {
    // println!("hello world");
    // other_file::function();
    // string::string_literal();
    // string::string_object();
    // shadowing::shadow();
    // functions::print_result();
    // functions::print_string();
    // flow_control::no_ternary_operator2();

    // ownership::drop_variable_1();
    // ownership::drop_variable_2();
    // ownership::lose_ownership();
    // ownership::get_ownership();
    // ownership::get_ownership_2();

    // slices::simple();
    // slices::vector();
    // oop::basics();
    // trait::
}