#[allow(dead_code)]
#[allow(unused_variables)]
pub fn simple() {
    let ints: [i32; 5] = [1, 2, 3, 4, 5];
    let slice1: &[i32] = &ints[0..2];
    let slice2: &[i32] = &ints[2..];

    println!("slice1: {:?}", slice1);
    println!("slice2: {:?}", slice2);
}

#[allow(dead_code)]
pub fn vector() {
    let v: Vec<i32> = vec![1,2,3,4,5,6,7,8];
    let slice1: &[i32] = &v[3..5];

    println!("slice1: {:?}", slice1);
}
