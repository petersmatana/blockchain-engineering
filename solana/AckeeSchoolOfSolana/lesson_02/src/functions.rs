#[allow(dead_code)]
fn get_bool(number: usize) -> bool {
    if number > 10 {
        true
    } else {
        false
    }
}

#[allow(dead_code)]
fn get_string() -> &'static str {
    "hello world"
}

#[allow(dead_code)]
pub fn print_result() {
    println!("result is: {}", get_bool(1));
}

#[allow(dead_code)]
pub fn print_string() {
    println!("result is: {}", get_string());
}
